import { writable } from 'svelte/store';

const exampleData: [string, string][] = [['two', 'tooltip text two']];

export default writable(new Map(exampleData));
